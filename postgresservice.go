package common

import (
	"fmt"
	"os"
	"sync"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" //Postgres required
	_ "github.com/jinzhu/gorm/dialects/sqlite"   //For testing
	logger "gitlab.com/service31/Common/Logger"
)

//DB Postgres Database
var DB *gorm.DB
var oncePG sync.Once

//InitDB initiate database
func InitDB() {
	oncePG.Do(func() {
		db, err := gorm.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
			os.Getenv("POSTGRES_HOST"), os.Getenv("POSTGRES_PORT"), os.Getenv("POSTGRES_USER"), os.Getenv("POSTGRES_DB"), os.Getenv("POSTGRES_PASSWORD")))
		logger.CheckFatal(err)
		DB = db
	})
}

//InitTestDB init test db
func InitTestDB() {
	oncePG.Do(func() {
		db, err := gorm.Open("sqlite3", "./test.sqlite3")
		logger.CheckFatal(err)
		DB = db
	})
}
