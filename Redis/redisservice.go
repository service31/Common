package redis

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"

	"github.com/go-redis/redis/v7"
	"github.com/vmihailenco/msgpack"
	logger "gitlab.com/service31/Common/Logger"
	module "gitlab.com/service31/Data/Module/Redis"
)

//RedisClient redis instance
var RedisClient *redis.Client
var onceRedis sync.Once

//RedisChangeKeys redis change keys
var RedisChangeKeys redisChangeKeys

//RedisMapKeys redis map keys
var RedisMapKeys redisMapKeys

//InitRedis initiate connection
func InitRedis() {
	onceRedis.Do(func() {
		RedisChangeKeys.Init()
		RedisMapKeys.Init()
		RedisClient = redis.NewClient(&redis.Options{
			Addr:     os.Getenv("REDIS_ADDR"),
			Password: os.Getenv("REDIS_PASSWORD"),
			DB:       getRedisDBNumber(),
		})
	})
}

//InitTestRedis for testing
func InitTestRedis(connectionString string) {
	onceRedis.Do(func() {
		RedisChangeKeys.Init()
		RedisMapKeys.Init()
		RedisClient = redis.NewClient(&redis.Options{
			Addr: connectionString,
		})
	})
}

func getRedisDBNumber() int {
	num, err := strconv.Atoi(os.Getenv("REDIS_DB"))
	logger.CheckFatal(err)
	return num
}

//GetRedisValue to interface
func GetRedisValue(key string, src interface{}) error {
	val, err := RedisClient.Get(key).Result()
	if err == redis.Nil || err != nil {
		return err
	}
	err = msgpack.Unmarshal([]byte(val), &src)
	if err != nil {
		return err
	}
	return nil
}

//GetRedisValues collection of interfaces
func GetRedisValues(key []string) ([]interface{}, error) {
	val, err := RedisClient.MGet(key...).Result()
	if err == redis.Nil || err != nil {
		return nil, err
	}
	return val, nil
}

//GetRedisHashValue get map value
func GetRedisHashValue(mapName, key string) (string, error) {
	val, err := RedisClient.HGet(mapName, key).Result()
	if err == redis.Nil || err != nil {
		return "", err
	}
	return val, nil
}

//GetRedisHashValues get collection of map values
func GetRedisHashValues(mapName string, key []string) ([]string, error) {
	var response []string
	val, err := RedisClient.HMGet(mapName, key...).Result()
	if err == redis.Nil || err != nil {
		return response, err
	}
	for _, v := range val {
		response = append(response, fmt.Sprintf("%v", v))
	}
	return response, nil
}

//GetRedisHashScanValues get redis hash scan values.
func GetRedisHashScanValues(mapName, key string) ([]string, error) {
	var response []string
	keys, _, err := RedisClient.HScan(mapName, 0, key, 0).Result()
	if err != nil {
		return nil, err
	}
	//The result is ONE slice of strings. Odd number is the value. Even number is the key itself
	for i := 1; i < len(keys); i += 2 {
		response = append(response, keys[i])
	}
	return response, nil
}

//PushItemChangedMessage push changed messages
func PushItemChangedMessage(property, ID string) error {
	item := &module.MessageItem{
		Property: strings.ToUpper(property),
		ID:       ID,
	}
	bytes, err := item.Marshal()
	if err != nil {
		return err
	}
	return RedisClient.Publish("ItemChange", bytes).Err()
}

//SetRedisValue set redis value
func SetRedisValue(key string, value []byte) error {
	return RedisClient.Set(key, value, 0).Err()
}

//SetRedisValues set redis values
func SetRedisValues(d map[string][]byte) error {
	return RedisClient.MSet(d).Err()
}

//SetRedisHashMapValue set redis hash map value
func SetRedisHashMapValue(mapName, key, value string) error {
	return RedisClient.HSet(mapName, key, value).Err()
}

//SetRedisHashMapValues set redis hash map values
func SetRedisHashMapValues(mapName string, value map[string]string) error {
	return RedisClient.HSet(mapName, value).Err()
}
