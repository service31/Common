package redis

import "fmt"

type redisChangeKeys struct {
	UserChange          string
	ServiceChange       string
	AddressChange       string
	ProductChange       string
	CategoryChange      string
	BusinessChange      string
	OrderChange         string
	QuestChange         string
	QuestProgressChange string
	PointsChange        string
	PromotionCodeChange string
	ShoppingCartChange  string
	MomentChange        string
}

type redisMapKeys struct {
	UserHashMap            string
	ServiceUsernameHashMap string
	AddressHashMap         string
	CityStateHashMap       string
	ProductHashMap         string
	CategoryHashMap        string
	BusinessHashMap        string
	OrderHashMap           string
	QuestHashMap           string
	QuestProgressHashMap   string
	PromotionCodeHashMap   string
	ShoppingCartHashMap    string
	MomentHashMap          string
}

func (r *redisChangeKeys) Init() {
	r.UserChange = "USER"
	r.ServiceChange = "SERVICE"
	r.AddressChange = "ADDRESS"
	r.ProductChange = "PRODUCT"
	r.CategoryChange = "CATEGORY"
	r.BusinessChange = "BUSINESS"
	r.OrderChange = "ORDER"
	r.QuestChange = "QUEST"
	r.QuestProgressChange = "QUEST_PROGRESS"
	r.PointsChange = "POINTS"
	r.PromotionCodeChange = "PROMOTION_CODE"
	r.ShoppingCartChange = "SHOPPING_CART"
	r.MomentChange = "MOMENT"
}

func (r *redisMapKeys) Init() {
	r.UserHashMap = "USER_MAP"
	r.ServiceUsernameHashMap = "SERVICE_USERNAME_MAP"
	r.AddressHashMap = "ADDRESS_MAP"
	r.CityStateHashMap = "CITYSTATE_MAP"
	r.ProductHashMap = "PRODUCT_MAP"
	r.CategoryHashMap = "CATEGORY_MAP"
	r.BusinessHashMap = "BUSINESS_MAP"
	r.OrderHashMap = "ORDER_MAP"
	r.QuestHashMap = "QUEST_MAP"
	r.QuestProgressHashMap = "QUEST_PROGRESS_MAP"
	r.PromotionCodeHashMap = "PROMOTION_CODE_MAP"
	r.ShoppingCartHashMap = "SHOPPING_CART_MAP"
	r.MomentHashMap = "MOMENT_MAP"
}

func (r *redisMapKeys) GetUserStringPropertyMapKey(serviceID, property string) string {
	return fmt.Sprintf("USER_Property_%s_%s", serviceID, property)
}

func (r *redisMapKeys) GetUserByServiceMapKey(serviceID, userID string) string {
	return fmt.Sprintf("USER_%s_%s", serviceID, userID)
}

func (r *redisMapKeys) GetUsersByServiceMapKey(serviceID string) string {
	return fmt.Sprintf("USER_%s_*", serviceID)
}

func (r *redisMapKeys) GetUserKey(ID string) string {
	return fmt.Sprintf("USER_%s", ID)
}

func (r *redisMapKeys) GetServiceMapKey(username string) string {
	return fmt.Sprintf("SERVICE_%s", username)
}

func (r *redisMapKeys) GetServiceKey(serviceID string) string {
	return fmt.Sprintf("SERVICE_%s", serviceID)
}

func (r *redisMapKeys) GetAddressKey(addressID string) string {
	return fmt.Sprintf("ADDRESS_%s", addressID)
}

func (r *redisMapKeys) GetDefaultAddressMapKey(entityID string) string {
	return fmt.Sprintf("ADDRESS_DEFAULT_%s", entityID)
}

func (r *redisMapKeys) GetAddressesByEntityMapKey(entityID string) string {
	return fmt.Sprintf("ADDRESS_%s_*", entityID)
}

func (r *redisMapKeys) GetAddressMapKey(entityID, ID string) string {
	return fmt.Sprintf("ADDRESS_%s_%s", entityID, ID)
}

func (r *redisMapKeys) GetCityStateKey(cityStateID string) string {
	return fmt.Sprintf("CITYSTATE_%s", cityStateID)
}

func (r *redisMapKeys) GetCityStateMapKey(state, city string) string {
	return fmt.Sprintf("CITYSTATE_%s_%s", state, city)
}

func (r *redisMapKeys) GetProductKey(id string) string {
	return fmt.Sprintf("PRODUCT_%s", id)
}

func (r *redisMapKeys) GetAllProductsByEntityMapKey(entityID string) string {
	return fmt.Sprintf("PRODUCT_%s_*", entityID)
}

func (r *redisMapKeys) GetProductMapKey(entityID, ID string) string {
	return fmt.Sprintf("PRODUCT_%s_%s", entityID, ID)
}

func (r *redisMapKeys) GetCategoryKey(id string) string {
	return fmt.Sprintf("CATEGORY_%s", id)
}

func (r *redisMapKeys) GetCategoriesByEntityMapKey(entityID string) string {
	return fmt.Sprintf("CATEGORY_%s_*", entityID)
}

func (r *redisMapKeys) GetCategoryMapKey(entityID, ID string) string {
	return fmt.Sprintf("CATEGORY_%s_%s", entityID, ID)
}

func (r *redisMapKeys) GetBusinessKey(ID string) string {
	return fmt.Sprintf("BUSINESS_%s", ID)
}

func (r *redisMapKeys) GetBusinessesByEntityMapKey(entityID string) string {
	return fmt.Sprintf("BUSINESS_%s_*", entityID)
}

func (r *redisMapKeys) GetBusinessMapKey(entityID, ID string) string {
	return fmt.Sprintf("BUSINESS_%s_%s", entityID, ID)
}

func (r *redisMapKeys) GetOrdersByEntityMapKey(entityID string) string {
	return fmt.Sprintf("ORDER_%s_*", entityID)
}

func (r *redisMapKeys) GetOrderMapKey(entityID, ID string) string {
	return fmt.Sprintf("ORDER_%s_%s", entityID, ID)
}

func (r *redisMapKeys) GetOrderKey(ID string) string {
	return fmt.Sprintf("ORDER_%s", ID)
}

func (r *redisMapKeys) GetQuestsByEntityMapKey(entityID string) string {
	return fmt.Sprintf("QUEST_%s_*", entityID)
}

func (r *redisMapKeys) GetQuestMapKey(entityID, ID string) string {
	return fmt.Sprintf("QUEST_%s_%s", entityID, ID)
}

func (r *redisMapKeys) GetProgressOfQuestMapKey(questID, ID string) string {
	return fmt.Sprintf("QUEST_%s_%s", questID, ID)
}

func (r *redisMapKeys) GetProgressesOfQuestByQuestKey(questID string) string {
	return fmt.Sprintf("QUEST_%s_*", questID)
}

func (r *redisMapKeys) GetQuestKey(ID string) string {
	return fmt.Sprintf("QUEST_%s", ID)
}

func (r *redisMapKeys) GetQuestProgressesByEntityMapKey(entityID string) string {
	return fmt.Sprintf("QUEST_PROGRESS_%s_*", entityID)
}

func (r *redisMapKeys) GetQuestProgressMapKey(entityID, ID string) string {
	return fmt.Sprintf("QUEST_PROGRESS_%s_%s", entityID, ID)
}

func (r *redisMapKeys) GetQuestProgressKey(ID string) string {
	return fmt.Sprintf("QUEST_PROGRESS_%s", ID)
}

func (r *redisMapKeys) GetPointsKey(entityID string) string {
	return fmt.Sprintf("POINTS_%s", entityID)
}

func (r *redisMapKeys) GetPromotionCodesByEntityMapKey(entityID string) string {
	return fmt.Sprintf("PROMOTION_CODE_%s_*", entityID)
}

func (r *redisMapKeys) GetPromotionCodeMapKey(entityID, ID string) string {
	return fmt.Sprintf("PROMOTION_CODE_%s_%s", entityID, ID)
}

func (r *redisMapKeys) GetPromotionCodeByPromotionCodeMapKey(entityID, promotionCode string) string {
	return fmt.Sprintf("PROMOTION_CODE_CODE_%s_%s", entityID, promotionCode)
}

func (r *redisMapKeys) GetPromotionCodeKey(ID string) string {
	return fmt.Sprintf("PROMOTION_CODE_%s", ID)
}

func (r *redisMapKeys) GetShoppingCartsByEntityMapKey(entityID string) string {
	return fmt.Sprintf("SHOPPING_CART_%s_*", entityID)
}

func (r *redisMapKeys) GetShoppingCartsBySpecificEntityAndBusinessMapKey(entityID, businessID string) string {
	return fmt.Sprintf("SHOPPING_CART_SPECIFIC_%s_%s", entityID, businessID)
}

func (r *redisMapKeys) GetShoppingCartMapKey(entityID, businessID string) string {
	return fmt.Sprintf("SHOPPING_CART_%s_%s", entityID, businessID)
}

func (r *redisMapKeys) GetShoppingCartKey(id string) string {
	return fmt.Sprintf("SHOPPING_CART_%s", id)
}

func (r *redisMapKeys) GetMomentKey(id string) string {
	return fmt.Sprintf("MOMENT_%s", id)
}

func (r *redisMapKeys) GetMomentEntityMapKey(entityID, id string) string {
	return fmt.Sprintf("MOMENT_%s_%s", entityID, id)
}

func (r *redisMapKeys) GetMomentsByEntityIDMapKey(entityID string) string {
	return fmt.Sprintf("MOMENT_%s_*", entityID)
}
