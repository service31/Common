package common

import (
	"github.com/alicebob/miniredis"
	logger "gitlab.com/service31/Common/Logger"
	redis "gitlab.com/service31/Common/Redis"
)

//Bootstrap User app
func Bootstrap() {
	logger.InitLogger()
	InitDB()
	redis.InitRedis()
}

//BootstrapTesting for testing
func BootstrapTesting(dbLogMode bool) {
	logger.InitLogger()
	InitTestDB()
	DB.LogMode(dbLogMode)

	mr, err := miniredis.Run()
	logger.CheckFatal(err)
	redis.InitTestRedis(mr.Addr())
}
