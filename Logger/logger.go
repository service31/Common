package logger

import (
	"log"
	"sync"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

//Logger logger instance
var logger *zap.Logger
var once sync.Once

// InitLogger initializes a thread-safe singleton logger
// This would be called from a main method when the application starts up
// This function would ideally, take zap configuration, but is left out
// in favor of simplicity using the example logger.
func InitLogger() {
	// once ensures the singleton is initialized only once
	once.Do(func() {
		config := zap.NewProductionConfig()
		config.EncoderConfig.EncodeTime = zapcore.RFC3339TimeEncoder
		config.OutputPaths = []string{"stdout"}
		l, _ := config.Build()
		logger = l
	})
}

// Debug logs a debug message with the given fields
func Debug(message string, fields ...zap.Field) {
	logger.Debug(message, fields...)
}

// Info logs a debug message with the given fields
func Info(message string, fields ...zap.Field) {
	logger.Info(message, fields...)
}

// Warn logs a debug message with the given fields
func Warn(message string, fields ...zap.Field) {
	logger.Warn(message, fields...)
}

// Error logs a debug message with the given fields
func Error(message string, fields ...zap.Field) {
	logger.Error(message, fields...)
}

// CheckError check error is nil or not
func CheckError(err error) bool {
	if err != nil {
		logger.Error(err.Error())
		return true
	}
	return false
}

// CheckRedisError this doesn't play well with zap
func CheckRedisError(err error) bool {
	if err != nil {
		log.Println(err)
		return true
	}
	return false
}

// Fatal logs a message than calls os.Exit(1)
func Fatal(message string, fields ...zap.Field) {
	logger.Fatal(message, fields...)
}

// CheckFatal check error is nil or not
func CheckFatal(err error) bool {
	if err != nil {
		logger.Fatal(err.Error())
		return true
	}
	return false
}

//GetLogger returns zap logger instance
func GetLogger() *zap.Logger {
	return logger
}
