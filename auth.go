package common

import (
	"context"
	"os"

	"github.com/dgrijalva/jwt-go"
	"github.com/gogo/status"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	uuid "github.com/satori/go.uuid"
	logger "gitlab.com/service31/Common/Logger"
	"google.golang.org/grpc/codes"
)

type claimKey string

const (
	//ClaimMetaKey claim key in meta
	ClaimMetaKey claimKey = "ClaimKey"
)

//JwtKey auth key
var JwtKey = []byte(os.Getenv("JWT_KEY"))

//Authenticate for grpc
func Authenticate(ctx context.Context) (context.Context, error) {
	token, err := grpc_auth.AuthFromMD(ctx, "bearer")
	if token == "" || logger.CheckError(err) {
		return nil, status.Error(codes.Unauthenticated, "Failed to get token")
	}

	claims, err := isTokenValid(token)

	if err != nil {
		return nil, err
	}
	newCtx := context.WithValue(ctx, ClaimMetaKey, claims)
	return newCtx, nil
}

func isTokenValid(token string) (*jwt.StandardClaims, error) {
	claims := &jwt.StandardClaims{}
	tkn, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
		return JwtKey, nil
	})

	if logger.CheckError(err) {
		return nil, status.Error(codes.Unauthenticated, "")
	}

	if !tkn.Valid || !claims.VerifyIssuer("zh-code.com", true) {
		return nil, status.Error(codes.Unauthenticated, "Invalid token")
	}

	uid := uuid.FromStringOrNil(claims.Id)
	if uid == uuid.Nil {
		return nil, status.Error(codes.Unauthenticated, "Invalid ID")
	}
	return claims, nil
}
