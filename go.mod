module gitlab.com/service31/Common

go 1.13

require (
	cloud.google.com/go v0.56.0 // indirect
	github.com/alicebob/gopher-json v0.0.0-20180125190556-5a6b3ba71ee6 // indirect
	github.com/alicebob/miniredis v2.5.0+incompatible
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/go-redis/redis/v7 v7.2.0
	github.com/gogo/googleapis v1.3.2 // indirect
	github.com/gogo/status v1.1.0
	github.com/golang/protobuf v1.4.0 // indirect
	github.com/gomodule/redigo v2.0.0+incompatible // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.0
	github.com/jinzhu/gorm v1.9.12
	github.com/lib/pq v1.4.0 // indirect
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.5.1 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible
	github.com/yuin/gopher-lua v0.0.0-20191220021717-ab39c6098bdb // indirect
	gitlab.com/service31/Data v0.0.0-20200422171134-418b93400238
	go.uber.org/zap v1.14.1
	golang.org/x/crypto v0.0.0-20200420201142-3c4aac89819a // indirect
	golang.org/x/net v0.0.0-20200421231249-e086a090c8fd // indirect
	golang.org/x/sys v0.0.0-20200420163511-1957bb5e6d1f // indirect
	golang.org/x/tools v0.0.0-20200422022333-3d57cf2e726e // indirect
	google.golang.org/genproto v0.0.0-20200420144010-e5e8543f8aeb // indirect
	google.golang.org/grpc v1.29.0
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
